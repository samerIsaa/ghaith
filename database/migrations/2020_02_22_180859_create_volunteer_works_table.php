<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVolunteerWorksTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('volunteer_works', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('image');
            $table->longText('description');
            $table->integer('count')->default(1)->comment('Volunteer Counts');
            $table->string('hours');
            $table->longText('conditions');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('volunteer_works');
    }
}
