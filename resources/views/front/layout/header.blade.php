<html>
<head>
    <title>Ghaith voluGhaith volunteer</title>
    <link rel="stylesheet" type="text/css" href="{{ asset('front/css.css') }}">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

</head>
<body>
<header>
    <div style="display: inline; font-size: 40px; float: right;">

        <a href="{{ url('/') }}"><img src="{{ asset('front/img/logo.png') }}" width="400" height="230"> </a>
    </div>

    <ul>
        <div style="float: left;">
            <li><a class="Lists" href="{{ route('advertising') }}">الاعلانات المتاحة حاليا</a></li>


            <li><a class="Lists" href="{{ url('/') }}">الرئيسية |</a></li>
            <li><a class="Lists" href="{{ url('/about') }}">نبذة |</a></li>
            <li class="dropdown"><a class="Lists" href="javascript:void(0)">الانضمام |</a>
                <div class="dropdown-content">
                    <a href="{{ route('company.register') }}">منظمة تطوعية</a>
                    <a href="volunteer.php">متطوع</a>
                    <a href="needy.php">محتاج</a>
                    <a href="login.php">دخول</a></div>
            </li>
        </div>
    </ul>


    <hr>
</header>
<div class="div">
    <br><br><br>
