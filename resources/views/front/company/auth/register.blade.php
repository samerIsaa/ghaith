@include('front.layout.header')
<center>

    <center>
        <table class="div2" width="70%">

            <tr>
                <td colspan="2">
                    <center>
                        @if ($errors->any())
                            <div class="alert alert-danger">{{ $errors->first() }}</div>
                        @endif
                    </center>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <center><h1>تسجيل جديد كمنظمة</h1></center>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <form action="{{ route('company.register') }}" method="POST" enctype="multipart/form-data">
                </td>
            </tr>
            @csrf
            <tr>
                <td> الاسم :</td>
                <td><input type="text" name="name" value="{{ old('name') }}" placeholder=" " required></td>
            </tr>
            <tr>
                <td> البريد الالكتروني :</td>
                <td><input type="email" name="email" value="{{ old('email') }}" placeholder=""></td>
            </tr>

            <tr>
                <td>المدينة :</td>
                <td><input type="text" name="city" placeholder="" value="{{ old('city') }}" required></td>
            </tr>
            <tr>
                <td> الموقع :</td>
                <td>
                    <input class="input100" id="loCation" type="text" required name="location" placeholder="">
                    <div id="map" style="width: 50%;height: 500px;overflow: hidden;position: relative;"></div>
                </td>
            </tr>
            <tr>
                <td>مجال العمل :</td>
                <td><input type="text" name="specialization" value="{{ old('specialization') }}" placeholder="" required></td>
            </tr>
            <tr>
                <td>السجل التجاري  :</td>
                <td><input type="text" name="commericalNo" value="{{ old('commericalNo') }}" placeholder="" required></td>
            </tr>
            <tr>
                <td>الموقع الالكتروني:</td>
                <td><input type="text" name="website" value="{{ old('website') }}" placeholder="" required></td>
            </tr>
            <tr>
                <td>نبذة:</td>
                <td><input type="text" name="info" value="{{ old('info') }}" placeholder="" required></td>
            </tr>
            <tr>
                <td>الشعار:</td>
                <td><input type="file" name="logo" placeholder="" required></td>
            </tr>
            <tr>
                <td>كلمة السر:</td>
                <td><input type="password" name="password" placeholder="" required></td>
            </tr>
            <tr>
                <td>تأكيد كلمة السر:</td>
                <td><input type="password" name="password_confirmation" placeholder="" required></td>
            </tr>
            <tr>
                <td colspan="2">
                    <center><input type="submit" name="Signup" value="تسجيل" class="submit"></center>
                </td>
            </tr>
            </form>


        </table>
    </center>

</center>


@include('front.layout.footer')

<script>
    var map;

    function initialize() {

        var myLatlng = new google.maps.LatLng(24.7244553, 46.542343);

        var myOptions = {

            zoom: 8,

            center: myLatlng,

            mapTypeId: google.maps.MapTypeId.ROADMAP

        };

        map = new google.maps.Map(document.getElementById("map"), myOptions);

        var marker = new google.maps.Marker({

            draggable: true,

            position: myLatlng,

            map: map,

            title: "Your location"

        });

        google.maps.event.addListener(marker, 'dragend', function (event) {

            document.getElementById("loCation").value = event.latLng.lat() + ',' + event.latLng.lng();

            infoWindow.open(map, marker);

        });

    }

    google.maps.event.addDomListener(window, "load", initialize());
</script>
<script async defer
        src="https://maps.googleapis.com/maps/api/js?key=AIzaSyA9UeezZ2xyNjrwck8SLdh9NxsJp6HhLQc&callback=initialize">
</script>
