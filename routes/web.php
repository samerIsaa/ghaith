<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('image/{folder}/{path}/{size?}', 'Controller@photo');


Route::get('/', function () {
    return view('front.index');
});



Route::get('about' , ['as' => 'about' , 'uses'=>'Front\HomeController@about']);
Route::get('advertising' , ['as' => 'advertising' , 'uses'=>'Front\AdvertisingController@index']);


Route::group(['prefix' => 'company' , 'as' => 'company.' , 'namespace' => 'Front\Company'], function () {

    Route::group(['namespace' => 'Auth'], function () {
        Route::get('/register', ['as' => 'register' , 'uses' => 'RegisterController@showRegistrationForm']);
        Route::post('/register', ['as' => 'register' , 'uses' => 'RegisterController@register']);



        Route::get('logout', ['as' => 'logout' , 'uses' => 'LoginController@logout']);
    });

    Route::group(['middleware' => 'auth:company'], function () {
        Route::get('/index', ['as' => 'index' , 'uses' => 'CompanyController@index']);

    });




});
