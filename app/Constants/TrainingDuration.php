<?php
/**
 * Created by PhpStorm.
 * User: Samer
 * Date: 18/11/2019
 * Time: 9:32 ص
 */

namespace App\Constants;


class TrainingDuration
{
    const ONE_MONTH = 0;
    const THREE_MONTH = 1;
    const SIX_MONTH = 1;
    const ANNUAL = 1;

}