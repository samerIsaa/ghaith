<?php
/**
 * Created by PhpStorm.
 * User: Samer
 * Date: 17/11/2019
 * Time: 1:35 م
 */

namespace App\Constants;


class OrderStatus
{
    const UNKNOWN = 0;
    const ACCEPTED = 1;
    const REJECTED = 2;
}