<?php
/******************************************************************************
 * Copyright (c) 2018.                                                        *
 * Created by PhpStorm.                                                       *
 * Developer: Mahmoud Rayan                                                   *
 * Date: 2017/02/25                                                           *
 * Time: $today.hour-6                                                        *
 ******************************************************************************/



namespace App\Constants;

class TrainingType {

    const SUMMER = 0;
    const WINTER = 1;

}