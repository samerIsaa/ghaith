<?php
/**
 * Created by PhpStorm.
 * User: Samer
 * Date: 17/11/2019
 * Time: 3:15 م
 */

namespace App\Constants;


class EducationAverage
{
    const ACCEPTABLE= 0;
    const GOOD = 1;
    const VERYGOOD = 2;
    const EXCELLENT = 3;
}