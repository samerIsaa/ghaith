<?php
/**
 * Created by PhpStorm.
 * User: Samer
 * Date: 17/11/2019
 * Time: 1:35 م
 */

namespace App\Constants;


class EmployeeType
{
    const FULLTIME = 0;
    const PARTTIME = 1;
}