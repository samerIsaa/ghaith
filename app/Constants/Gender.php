<?php
/**
 * Created by PhpStorm.
 * User: Samer
 * Date: 17/11/2019
 * Time: 2:12 م
 */

namespace App\Constants;


class Gender
{
    const MALE = 0;
    const FEMALE = 1;
}