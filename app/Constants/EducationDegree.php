<?php
/**
 * Created by PhpStorm.
 * User: Samer
 * Date: 17/11/2019
 * Time: 3:15 م
 */

namespace App\Constants;


class EducationDegree
{
    const DOPLOMA= 0;
    const BACALORIA = 1;
    const MASTER = 2;
    const PHD = 3;
}