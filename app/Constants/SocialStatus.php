<?php
/**
 * Created by PhpStorm.
 * User: Samer
 * Date: 17/11/2019
 * Time: 1:32 م
 */

namespace App\Constants;


class SocialStatus
{
    const MARRIED = 0;
    const SINGLE = 1;
}