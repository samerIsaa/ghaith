<?php

namespace App\Http\Controllers\Front\Company;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class CompanyController extends Controller
{
    public function index()
    {
        return view('front.company.index');
    }
}
